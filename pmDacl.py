import os
import wmi
import win32con
import win32security
import sys
import win32api
import ntsecuritycon as con

# The following table lists the values for file and directory access rights in the AccessMask property.
# This property is a bitmap.
ACCESSMASKSPROPERTY = ['FILE_READ_DATA', 'FILE_LIST_DIRECTORY', 'FILE_WRITE_DATA', 'FILE_ADD_FILE', 
                     'FILE_APPEND_DATA', 'FILE_ADD_SUBDIRECTORY', 'FILE_CREATE_PIPE_INSTANCE',
                     'FILE_READ_EA', 'FILE_WRITE_EA', 'FILE_EXECUTE', 'FILE_TRAVERSE', 'FILE_DELETE_CHILD', 
                     'FILE_READ_ATTRIBUTES', 'FILE_WRITE_ATTRIBUTES', 'FILE_ALL_ACCESS', 'FILE_GENERIC_READ',
                     'FILE_GENERIC_WRITE', 'FILE_GENERIC_EXECUTE'] 

# Access Control Entries (ACE)
#https://msdn.microsoft.com/en-us/library/windows/desktop/aa374919(v=vs.85).aspx
ACETYPES = ['ACCESS_ALLOWED_ACE_TYPE','ACCESS_ALLOWED_CALLBACK_ACE_TYPE','ACCESS_ALLOWED_CALLBACK_OBJECT_ACE_TYPE',
            'ACCESS_ALLOWED_OBJECT_ACE_TYPE','ACCESS_DENIED_ACE_TYPE','ACCESS_DENIED_CALLBACK_ACE_TYPE',
            'ACCESS_DENIED_CALLBACK_OBJECT_ACE_TYPE','ACCESS_DENIED_OBJECT_ACE_TYPE','ACCESS_MAX_MS_ACE_TYPE',
            'ACCESS_MAX_MS_V2_ACE_TYPE','ACCESS_MAX_MS_V3_ACE_TYPE','ACCESS_MAX_MS_V4_ACE_TYPE','ACCESS_MAX_MS_OBJECT_ACE_TYPE',
            'ACCESS_MIN_MS_ACE_TYPE','ACCESS_MIN_MS_OBJECT_ACE_TYPE','SYSTEM_ALARM_ACE_TYPE','SYSTEM_ALARM_CALLBACK_ACE_TYPE',
            'SYSTEM_ALARM_CALLBACK_OBJECT_ACE_TYPE','SYSTEM_ALARM_OBJECT_ACE_TYPE','SYSTEM_AUDIT_ACE_TYPE','SYSTEM_AUDIT_CALLBACK_ACE_TYPE',
            'SYSTEM_AUDIT_OBJECT_ACE_TYPE','SYSTEM_MANDATORY_LABEL_ACE_TYPE','ACCESS_ALLOWED_COMPOUND_ACE_TYPE','SYSTEM_AUDIT_CALLBACK_OBJECT_ACE_TYPE',
            'ACCESS_MAX_MS_V5_ACE_TYPE']

def logAuditing(msg):
    fd = open("logAuditingCreation.txt","at")
    s = str(msg)
    fd.write(s)
    fd.write("\n")
    fd.close()
    return
    
def getAccessMask(accessMask):
    for entryMask in ACCESSMASKSPROPERTY:
        entryMaskAttr = getattr(con, entryMask)
        if (entryMaskAttr & accessMask) == entryMaskAttr:
            yield entryMask
            
# get the the acces control entry for a give ACE 
def getAceTypes(aceType):
    for entry in ACETYPES:
        if getattr(con, entry) == aceType:
            yield entry

     
def processMonitor(winManInterface):
    print("\n........Process monitoring....\n")
    print("Image Name\tPID\tUser\tExecutatble\tCreation Date\t Timestamp\n")
    print("==========\t===\t====\t============\t===========\t===========\n")
    
    win32ProcessWatchCreation = winManInterface.Win32_Process.watch_for("creation")
      
    while True:

        try:
            processExecutablePath = ""
            processWatcher = win32ProcessWatchCreation()
           
            processOwner = processWatcher.GetOwner()
           
            msg1 = processWatcher.Name + "\t"+ str(processWatcher.ProcessId)
            
            msg2 = "(\tOwner-Unknown \t)"
            try:
                msg2 = "\t(" + processOwner[0] + " [" + processOwner[2] + "])\t"
            except:
                msg2 = "(\tOwner-Unknown \t)"
                
            msg3 = "Unknown Path\t"
            try:
                msg3 = processWatcher.ExecutablePath + "\t"
                processExecutablePath = processWatcher.ExecutablePath
                
            except:
                msg3 = "Unknown Path\t"

            msg4 = "Creation Date Unknown\t"
            try:
                msg4 = str(processWatcher.CreationDate) + "\t"
                
            except:
                msg4 = "Creation Date Unknown\t"

            msg5 = "NO timestamp"
            try:
                msg5 = str(processWatcher.timestamp)
            except:
                msg5 = "NO timestamp"
                
  
            msgAll = str(msg1) + str(msg2) + str(msg3) + str(msg4) + str(msg5) 
            
            print(msgAll)
            logAuditing(msgAll)
            displayFileAccessControlList(processExecutablePath)
        except KeyboardInterrupt:
            print("\nBreak out -[Creation] \n")
            sys.exit()
        except:
            print("\ncontinue... [Creation] \n")
            pass
        
def listRunningProcess(winInterface):
    print("\n*******************************************************************")
    print(".......Printing running processes.....")
    print("*******************************************************************\n")
    #print("Process Name\t Process ID\n")
    for process in winInterface.Win32_Process ():
        msg1 = "Process Name: " +process.Name + "\t PID: "+ str(process.ProcessId)
        processPathName = ""
        try:
            
            processPathName = process.ExecutablePath
        except:
            print("PATH_NOT_KNOW")
        print(msg1)
        logAuditing(msg1)
        displayFileAccessControlList(processPathName)
        print('<------------------------------------>')
        
    
    print("*******************************************************************\n")





def displayFileAccessControlList(fileName):
    try:
        #msgData = []
        securityDescriptor = win32security.GetFileSecurity(fileName, win32security.DACL_SECURITY_INFORMATION)

        securityDescriptorDacl = securityDescriptor.GetSecurityDescriptorDacl()     

        aceCount = securityDescriptorDacl.GetAceCount()
        
        print('File: ', fileName)
        print('Access Control Entries : ('+str(aceCount)+')')
        msgData1 = 'File: '+fileName
        logAuditing(msgData1)
        msgData2 = 'Access Control Entries : ('+str(aceCount)+')'
        logAuditing(msgData2)
        
        for counter in range(0, aceCount):
            (aceType, aceFlag), accessMask, userSID = securityDescriptorDacl.GetAce(counter)
            
            user, group, userType = win32security.LookupAccountSid('', userSID)

            msgData1 = str('User: {}\\{}').format(group, user)
            msgData2 = str('ACE Type ({}):').format(aceType), '; '.join(getAceTypes(aceType))
            msgData3 = str('Access Mask ({}):').format(accessMask), ' | '.join(getAccessMask(accessMask))
            
           
            print(msgData1)
            print('[+}',msgData2)
            print('[+]',msgData3)
            print('')
            
            logAuditing(msgData1)
            logAuditing(msgData2)
            logAuditing(msgData3)
            #logAuditing(msgData4)
    except:
        print('FILE_ACCESS_FAILED - File Path Not Available')
        
def main():
    print('*')
    winInterface = wmi.WMI()
    if len(sys.argv)  == 2:
        #type> python pmDacl.py fileName
        fileName = sys.argv[1]
        print('Processing file...['+fileName+']')
        print('')
        displayFileAccessControlList(fileName)
    else:    
        listRunningProcess(winInterface)
        processMonitor(winInterface)
    print('*')

    
if __name__ == "__main__":
    main()
